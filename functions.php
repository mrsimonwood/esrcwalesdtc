<?php
/**
 * Doctoral Training functions and definitions
 *
 * @package Doctoral_Training
 * @since Doctoral_Training 0.1
 */

/**
 * Sets up all the additional sidebars
 *
 * @since Doctoral_Training 0.1
 */

function esrcwalesdtc_sidebars() {
	register_sidebar( array(
		'name' => __( 'Social Widgets', 'esrcwalesdtc' ),
		'id' => 'sidebar-9',
		'description' => __( 'Widgets for sharing and following', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Front Page Sidebar One', 'esrcwalesdtc' ),
		'id' => 'sidebar-6',
		'description' => __( 'The sidebar for the front page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Front Page Sidebar Two', 'esrcwalesdtc' ),
		'id' => 'sidebar-7',
		'description' => __( 'The sidebar for the front page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Front Page Sidebar Three', 'esrcwalesdtc' ),
		'id' => 'sidebar-8',
		'description' => __( 'The sidebar for the front page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( '404 Sidebar One', 'esrcwalesdtc' ),
		'id' => 'sidebar-10',
		'description' => __( 'The sidebar for the 404 page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( '404 Sidebar Two', 'esrcwalesdtc' ),
		'id' => 'sidebar-11',
		'description' => __( 'The sidebar for the 404 page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( '404 Sidebar Three', 'esrcwalesdtc' ),
		'id' => 'sidebar-12',
		'description' => __( 'The sidebar for the 404 page', 'esrcwalesdtc' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'wp_loaded', 'esrcwalesdtc_sidebars' );

/**
 * Sets the logo (header image) height to 42px
 *
 * @since Doctoral_Training 0.1
 */

function dtc_header_height($param)
{
	return 42;
}
add_filter( 'twentyeleven_header_image_height', 'dtc_header_height');

/**
 * Sets the logo (header image) width to 400px
 *
 * @since Doctoral_Training 0.1
 */

function dtc_header_width($param) 
{
	return 400;
}
add_filter( 'twentyeleven_header_image_width', 'dtc_header_width');

/**
 * Adds additional image sizes - the banner (featured image on single posts) and 'mini'
 * and 'small-thumb' sizes used primarily for profile images.
 *
 * @since Doctoral_Training 0.1
 */

function additional_image_size() {
      if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'banner', 1000, 191, true ); 
        add_image_size( 'mini', 48, 48, true ); 
        add_image_size( 'small-thumb', 75, 75, true ); 
    }
}
add_action( 'after_setup_theme', 'additional_image_size' );

/**
 * Removes the Twenty Eleven Options page, since this child theme makes many redundant
 *
 * @since Doctoral_Training 0.2
 */

function remove_twenty_eleven_actions() {
	remove_action( 'admin_init', 'twentyeleven_theme_options_init');
	remove_action( 'admin_menu', 'twentyeleven_theme_options_add_page' );
	update_option( 'twentyeleven_theme_options', twentyeleven_get_default_theme_options() );
}
add_action('init','remove_twenty_eleven_actions');

/**
 * Fix the options that Twenty Eleven offers to suit this child theme
 *
 * @since Doctoral_Training 0.2
 */

function update_twenty_eleven_options($default_theme_options) {
	$default_theme_options = array(
		'color_scheme' => 'light',
		'link_color'   => twentyeleven_get_default_link_color( 'light' ),
		'theme_layout' => 'content',
	);
	return $default_theme_options;
}
add_filter('twentyeleven_default_theme_options', 'update_twenty_eleven_options');

/**
 * Add our own options for the Doctoral Training theme - specifically, users can
 * set the colour of the menu bar, the colour of <h> elements, and set the last
 * menu item to align right - we use this for linking to alternate language version
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_theme_options_init() {
	register_setting(
		'wdtc_theme_options', 'wdtc_theme_options', 'wdtc_theme_options_validate' 
	);

	// Register our settings field group
	add_settings_section(
		'general', 				// Unique identifier for the settings section
		'', 					// Section title (we don't want one)
		'__return_false', 		// Section callback (we don't want anything)
		'wdtc_theme_options' 	// Menu slug, used to uniquely identify the page;
	);

	// Register our individual settings fields
	add_settings_field( 'menu_bar_color', __( 'Menu Bar Color', 'esrcwalesdtc' ), 'wdtc_settings_field_menu_bar_color', 'wdtc_theme_options', 'general' );
	add_settings_field( 'headings_color', __( 'Headings Color', 'esrcwalesdtc' ), 'wdtc_settings_field_headings_color', 'wdtc_theme_options', 'general' );
	add_settings_field( 'last_menu_item_right', __( 'Last Menu Item on the Right?', 'esrcwalesdtc' ), 'wdtc_settings_field_last_menu_item_right', 'wdtc_theme_options', 'general' );
	add_settings_field( 'fallback_image', __( 'Fallback image URL', 'esrcwalesdtc' ), 'wdtc_settings_field_fallback_image', 'wdtc_theme_options', 'general' );
}
add_action( 'admin_init', 'wdtc_theme_options_init' );

/**
 * Callback for register_setting to validate the options chosen
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_theme_options_validate($input) {
	$output = wdtc_get_default_theme_options();
	$options = wdtc_get_theme_options();
    $output['menu_bar_color'] = $options['menu_bar_color'];
    $output['headings_color'] = $options['headings_color'];
	// Colours must be 3 or 6 hexadecimal characters
	if ( isset( $input['menu_bar_color'] ) && preg_match( '/^#?([a-f0-9]{3}){1,2}$/i', $input['menu_bar_color'] ) )
		$output['menu_bar_color'] = '#' . strtolower( ltrim( $input['menu_bar_color'], '#' ) );
	if ( isset( $input['headings_color'] ) && preg_match( '/^#?([a-f0-9]{3}){1,2}$/i', $input['headings_color'] ) )
		$output['headings_color'] = '#' . strtolower( ltrim( $input['headings_color'], '#' ) );
	if ( isset( $input['last_menu_item_right'] ) )
		$output['last_menu_item_right'] = $input['last_menu_item_right'];
	if ( isset( $input['fallback_image'] ) )
		$output['fallback_image'] = esc_url($input['fallback_image'], array('http', 'https'));
	return $output;
}

/**
 * Callback for add_settings_field to render the menu bar color field
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_settings_field_menu_bar_color() {
	$options = wdtc_get_theme_options();
	?>
	<input type="text" name="wdtc_theme_options[menu_bar_color]" id="menu-bar-color" value="<?php echo esc_attr( $options['menu_bar_color'] ); ?>" />
	<br />
	<?php

}

/**
 * Callback for add_settings_field to render the headings color field
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_settings_field_headings_color() {
	$options = wdtc_get_theme_options(); ?>
	<input type="text" name="wdtc_theme_options[headings_color]" id="headings-color" value="<?php echo esc_attr( $options['headings_color'] ); ?>" />
	<br />
	<?php

}

/**
 * Callback for add_settings_field to render last menu item right checkbox
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_settings_field_last_menu_item_right() {
	$options = wdtc_get_theme_options();
	?>
	<input type="checkbox" name="wdtc_theme_options[last_menu_item_right]" id="last_menu_item_right" <?php if ($options['last_menu_item_right'] ) echo 'checked'; ?>/>
	<br />
	<?php
}

/**
 * Callback for add_settings_field to render the fallback image field
 *
 * @since Doctoral_Training 0.2.4
 */

function wdtc_settings_field_fallback_image() {
	$options = wdtc_get_theme_options(); ?>
	<input type="text" name="wdtc_theme_options[fallback_image]" id="fallback-image" value="<?php echo esc_attr( $options['fallback_image'] ); ?>" />
	<br />
	<?php

}

/**
 * Theme options admin page
 *
 * @since Doctoral_Training 0.2
 */
 
function wdtc_theme_options_add_page() {
	$theme_page = add_theme_page(
		__( 'Theme Options', 'esrcwalesdtc' ),   // Name of page
		__( 'Theme Options', 'esrcwalesdtc' ),   // Label in menu
		'edit_theme_options',            // Capability required
		'wdtc_theme_options',            // Menu slug, used to uniquely identify the page
		'wdtc_theme_options_render_page' // Function that renders the options page
	);
	if ( ! $theme_page )
		return;
	add_action( "load-$theme_page", 'twentyeleven_theme_options_help' );
}
add_action( 'admin_menu', 'wdtc_theme_options_add_page' );

/**
 * Callback for add_theme_page to render the options page
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_theme_options_render_page() {
	?>
	<div class="wrap">
		<?php screen_icon(); ?>
		<?php $theme_name = function_exists( 'wp_get_theme' ) ? wp_get_theme() : get_current_theme(); ?>
		<h2><?php printf( __( '%s Theme Options', 'esrcwalesdtc' ), $theme_name ); ?></h2>
		<?php settings_errors(); ?>

		<form method="post" action="options.php">
			<?php
				settings_fields( 'wdtc_theme_options' );
				do_settings_sections( 'wdtc_theme_options' );
				submit_button();
			?>
		</form>
	</div>
	<?php
}

/**
 * Use options to style the colour of the menu bar, the colour of <h> elements, 
 * and (if selected) set the last menu item to align right
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_style_adjustments() {
	wp_enqueue_style(
		'wdtc-style',
		get_stylesheet_uri()
	);
	$options = wdtc_get_theme_options();
    $mbcolor = $options['menu_bar_color'];
    $headingcolor = $options['headings_color'];
    $adjustedments_css = "#access2 {
			background: $mbcolor; /* Show a solid color for older browsers */
			background: -moz-linear-gradient($mbcolor, #0a0a0a);
			background: -o-linear-gradient($mbcolor, #0a0a0a);
			background: -webkit-gradient(linear, 0% 0%, 0% 100%, from($mbcolor), to(#0a0a0a)); /* older webkit syntax */
			background: -webkit-linear-gradient($mbcolor, #0a0a0a);
		}
		.entry-content h3,
		.comment-content h3,
		.term-description h3,
		.further-information h3,
		h3.archive-subhead, 
		#profiles h3 {
			color: $headingcolor;
		}
		.entry-content h2 { 
			color: $headingcolor;
		}
		.highlight, .grey-box {
			border-color: $mbcolor;
		}
		";
	if ( $options['last_menu_item_right'] )
		$adjustedments_css .= "#access2 ul ul li:last-child {
			float: right;
		}";
	wp_add_inline_style( 'wdtc-style', $adjustedments_css );
}
add_action( 'wp_enqueue_scripts', 'wdtc_style_adjustments' );

/**
 * Get the theme options for this theme, with defaults
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_get_theme_options() {
	return get_option( 'wdtc_theme_options', wdtc_get_default_theme_options() );
}

/**
 * Defaults for menu bar color (grey), whether last menu item is on the right (no)
 *
 * @since Doctoral_Training 0.2
 */

function wdtc_get_default_theme_options() {
	return array('menu_bar_color' => '#373737', 'last_menu_item_right' => false);
}

/**
 * Returns the URL 
 *
 * @since Doctoral_Training 0.2.4
 */

function wdtc_get_default_thumbnail( $bannersize = false ) {
	$options = wdtc_get_theme_options();
	$image_url = esc_url($options['fallback_image']);
	// If the fallback image isn't set in the options, or isn't a file, use the
	// one supplied with the theme
	$image_size = @getimagesize ($image_url);
	if ($image_url == '' || !$image_size)
		$image_url = get_stylesheet_directory_uri() . '/images/default.jpg';
	// If it should be banner size, get the dimensions this image ought to be
	if ( $bannersize ) {
		global $_wp_additional_image_sizes;
		$width = $_wp_additional_image_sizes['banner']['width'];
		$height = $_wp_additional_image_sizes['banner']['height'];
		// Check if the image is the correct dimensions
 		if ( ($image_size[0] == $width) && ($image_size[1] == $height) )
 			return $image_url;
 		// If the image is not of the correct dimensions, check if there's a
 		// version using WP's convention of appending -wxh to the basename
 		$extension_pos = strrpos($image_url, '.');
		$image_url = substr($image_url, 0, $extension_pos) . '-' . $width . 'x' . $height . substr($image_url, $extension_pos);
		$image_size = @getimagesize ($image_url);
		if (!$image_size)
			return;
		if ( ($image_size[0] == $width) && ($image_size[1] == $height) )
			return $image_url;
	}
	// Otherwise, return it anyway
	return $image_url;
}	

/**
 * Metadata for Facebook Open Graph
 *
 * @since Doctoral_Training 0.1
 */

function facebook_og() {
	global $post;
	if ( !is_singular() || !($post->ID)) //if it is not a post or a page
		return;
	// Image for FB
	$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'large');
	if ($img){
		echo '<meta property="og:image" content="' . $img[0] . '"/>';
	} else {
		echo '<meta property="og:image" content="' . wdtc_get_default_thumbnail() . '"/>';		
	} 
	// Other Facebook stuff...
	echo '<meta property="og:url" content="' . get_permalink() . '"/>';
	echo '<meta property="og:title" content="' . get_the_title() . '"/>';
 	if (!$excerpt = trim($post->post_excerpt)) {
      		$excerpt = $post->post_content;
    		$excerpt = strip_shortcodes( $excerpt );
        	$excerpt = apply_filters('the_content', $excerpt);
        	$excerpt = str_replace(']]>', ']]>', $excerpt);
        	$excerpt = strip_tags($excerpt);
        	$excerpt_length = 55;
	        $words = preg_split("/[\n\r\t ]+/", $excerpt, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
        	if ( count($words) > $excerpt_length ) {
            		array_pop($words);
            		$excerpt = implode(' ', $words);
   		} else {
            		$excerpt = implode(' ', $words);
        	}
    	}
	echo '<meta property="og:description" content="' . $excerpt . '"/>';
}
add_action( 'wp_head', 'facebook_og');

/**
 * Show the logo (header image) on the login screen
 *
 * @since Doctoral_Training 0.1
 */
  
function custom_login_logo()
{
	if ( function_exists( 'get_custom_header' ) ) {
		$header_image_width  = get_custom_header()->width;
		$header_image_height = get_custom_header()->height;
	} else {
		$header_image_width  = HEADER_IMAGE_WIDTH;
		$header_image_height = HEADER_IMAGE_HEIGHT;
	}
	$marginleft = (326 - $header_image_width)/2;
    echo '<style  type="text/css"> h1 a {  background-image:url(' . get_header_image() . ')  !important; ';
    echo 'border: 1px solid #e5e5e5; ';
    echo '-webkit-box-shadow: rgba(200,200,200,0.7) 0 4px 10px -1px; ';
    echo 'box-shadow: rgba(200,200,200,0.7) 0 4px 10px -1px; ';
    echo 'padding: 0 !important; ';
    echo 'background-size: auto !important; ';
    echo 'margin-bottom: 30px !important; ';
    echo 'margin-left: ' . $marginleft . 'px !important; ';
    echo 'width: ' . $header_image_width . 'px !important; ';
    echo 'height: ' . $header_image_height . 'px !important; } </style>';
}
add_action('login_head',  'custom_login_logo');

//This contains code associated with our Google Account (for CSE).
//TO DO: Take this out and put it in a separate plugin.
function wdtc_searchbox() {
	$gcsecode = '<div id="search-box">';
	$gcsecode .= '<script>';
	$gcsecode .= '(function() {';
	$gcsecode .= '    var cx = \'009906827011079005043:t3o381d-hvo\';';
	$gcsecode .= '    var gcse = document.createElement(\'script\');';
	$gcsecode .= '    gcse.type = \'text/javascript\';';
	$gcsecode .= '    gcse.async = true;';
	$gcsecode .= '    gcse.src = \'https://cse.google.com/cse.js?cx=\' + cx;';
	$gcsecode .= '    var s = document.getElementsByTagName(\'script\')[0];';
	$gcsecode .= '    s.parentNode.insertBefore(gcse, s);';
	$gcsecode .= '  })();';
	$gcsecode .= '</script>';
	$gcsecode .= '<gcse:searchbox-only>Loading</gcse:searchbox-only>';
	$gcsecode .= '</div>';
	echo $gcsecode;
}
add_action('wdtc_searchbox','wdtc_searchbox');

/**
 * A latest posts widget featuring thumbnails
 *
 * @since Doctoral_Training 0.1
 */

class Latest_Posts_With_Thumbnail_Widget extends WP_Widget {
          function __construct() {
                    $widget_ops = array(
                    'classname' => 'latest_posts_with_thumbnail_widget',
                    'description' => 'Gets latests posts with thumbnail'
          );
          parent::__construct(
                    'Latest_Posts_With_Thumbnail_Widget',
                    'Latest Posts with Thumbnail',
                    $widget_ops
          );
}
	function widget($args, $instance) { // widget sidebar output
		extract($args, EXTR_SKIP);
		echo $before_widget; // pre-widget code from theme

		echo '<h3 class="widget-title">' . __('Latest News', 'esrcwalesdtc') . '</h3>';
		echo '<ul>';
		$args = array( 'numberposts' => $instance['number_of_posts'], 'post_status' => 'publish');
		if ($instance['any_post_type']) $args['post_type'] = 'any';
		if ($instance['restrict_to_cat']) $args['category'] = $instance['cat'];
		$recent_posts = wp_get_recent_posts( $args );
		foreach( $recent_posts as $recent ){
			echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' . get_the_post_thumbnail($recent["ID"], 'mini', array('class' => "alignleft")) . '</a><p><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a></p></li> ';
		}
		echo '</ul>';

		echo $after_widget; // post-widget code from theme
          }

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		//Strip tags from title and name to remove HTML 

		$instance['any_post_type'] = $new_instance['any_post_type'];
		$instance['restrict_to_cat'] = $new_instance['restrict_to_cat'];
		$instance['cat'] = $new_instance['cat'];
		$instance['number_of_posts'] = $new_instance['number_of_posts'];

		return $instance;
	}

	
	function form( $instance ) {

		//Set up some default widget settings.
		$defaults = array( 'any_post_type' => true, 'number_of_posts' => 3 );
		$instance = wp_parse_args( (array) $instance, $defaults );
		echo '<p>Show <select name="' . $this->get_field_name( 'number_of_posts' ) . '">';
		for ($i = 2; $i <= 10; $i++)
		{
			echo '<option value="' . $i . '"';
			if ($i == $instance['number_of_posts'])
				echo ' selected';
			echo '>' . $i . '</option>';
		}
		echo '</select> posts</p>';
		 ?>
		<p>You can display just posts, or other page types (eg. pages) too.</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'any_post_type' ); ?>"><?php _e('Use all post types?', 'esrcwalesdtc'); ?></label>
			<input class="checkbox" type="checkbox" <?php checked( isset( $instance['any_post_type']), true ); ?> id="<?php echo $this->get_field_id( 'any_post_type' ); ?>" name="<?php echo $this->get_field_name( 'any_post_type' ); ?>" /> 
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'restrict_to_cat' ); ?>"><?php _e('Restrict to a particular category?', 'esrcwalesdtc'); ?></label>
			<input class="checkbox" type="checkbox" <?php checked( isset( $instance['restrict_to_cat']), true ); ?> id="<?php echo $this->get_field_id( 'restrict_to_cat' ); ?>" name="<?php echo $this->get_field_name( 'restrict_to_cat' ); ?>" /> 
		</p>
		<p>
			<label>
				<?php _e( 'Category' ); ?>:
				<?php wp_dropdown_categories( array( 'name' => $this->get_field_name('cat'), 'selected' => $instance['cat'] ) ); ?>
			</label>
		</p>
		<?php 
	}

}
function init_latest_posts_with_thumbnail_widget()
{
	return register_widget("Latest_Posts_With_Thumbnail_Widget");
}
add_action('widgets_init', 'init_latest_posts_with_thumbnail_widget');

/**
 *
 * Adds thumbnail inline with content for profiles
 *
 *
 */

function add_thumbnail_to_the_content($content) {
	if ( is_singular() && has_post_thumbnail( $post->ID ) && get_post_type()=='profile')
		$content = get_the_post_thumbnail( null,  'thumbnail' , array('class' => "alignleft") ) . $content;
	return $content;
}
add_filter ('the_content', 'add_thumbnail_to_the_content');
?>