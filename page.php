<?php
/**
 * The template for displaying all pages.
 *
 * The Doctoral Training version adds sidebars on the front page
 * 
 * @package Doctoral_Training
 * @since Doctoral_Training 0.1
 */

get_header(); ?>

		<div id="primary">
			<div id="content" role="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ); ?>

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

<?php if (is_front_page()) { ?>
	
	<div id="front-page-widget-area">

	<div id="front-page-sidebar-1" class="front-page-sidebar">
	<?php if ( ! dynamic_sidebar( 'sidebar-6') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>
	<div id="front-page-sidebar-2" class="front-page-sidebar">
	<?php if ( ! dynamic_sidebar( 'sidebar-7') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>
	<div id="front-page-sidebar-3" class="front-page-sidebar">
	<?php if ( ! dynamic_sidebar( 'sidebar-8') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>

	</div>

<?php } ?>


			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_footer(); ?>