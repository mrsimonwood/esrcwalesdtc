<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * Doctoral_Training version adds additional sidebar for social widgets immediately
 * above the main footer sidebar
 *
 * @package Doctoral_Training
 * @since Doctoral_Training 0.1
 */
?>

	</div><!-- #main -->
	
	<?php if ( ! is_404() ) { ?>
	<footer id="colophon" role="contentinfo">

	<div id="social-widgets">

	<?php if ( ! dynamic_sidebar( 'sidebar-9') ) : ?>
	<?php endif; // end sidebar widget area ?>

	</div><!-- #social-widgets -->

	<?php } ?>
			<?php
				/* A sidebar in the footer? Yep. You can can customize
				 * your footer with three columns of widgets.
				 */
				if ( ! is_404() )
					get_sidebar( 'footer' );
			?>
	</footer><!-- #colophon -->
</div><!-- #page -->
<div id="credit"><a href="https://www.walesdtp.ac.uk/web/doctoral-training-theme/"><?php _e('Doctoral Training Wordpress Theme', 'esrcwalesdtc')?></a> <?php _e('by', 'esrcwalesdtc')?> <a href="http://www.simonwood.info">Simon Wood</a></div>

<?php wp_footer(); ?>

</body>
</html>