# README #

### What is this repository for? ###

* A theme optimised for a doctoral training centre (child theme for the Twenty Eleven)
* Version 0.2.23

### How do I get set up? ###

* Upload to wp-content/themes via FTP or Themes > Add New > Upload Theme
* In the dashboard choose "Activate"