<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * Doctoral_Training version adds three sidebars to the Twenty Eleven 404 
 *
 * @package Doctoral_Training
 * @since Doctoral_Training 0.1
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main">

			<article id="post-0" class="post error404 not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Sorry, we couldn&rsquo;t find that page', 'esrcwalesdtc' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps one of the links below can help.', 'esrcwalesdtc' ); ?></p>

	<div class="widget-area">
	<?php if ( ! dynamic_sidebar( 'sidebar-10') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>

	<div class="widget-area">
	<?php if ( ! dynamic_sidebar( 'sidebar-11') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>

	<div class="widget-area" id="right-widget-area">
	<?php if ( ! dynamic_sidebar( 'sidebar-12') ) : ?>
	<?php endif; // end sidebar widget area ?>
	</div>

				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>